package com.memrise.marvel.db;

import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;

import com.memrise.marvel.data.database.MarvelDatabase;

import org.junit.After;
import org.junit.Before;

abstract public class DbTest {
    protected MarvelDatabase db;

    @Before
    public void initDb() {
        db = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
                MarvelDatabase.class).build();
    }

    @After
    public void closeDb() {
        db.close();
    }
}
