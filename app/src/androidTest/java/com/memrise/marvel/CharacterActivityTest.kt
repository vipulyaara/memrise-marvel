package com.memrise.marvel

import android.app.Application
import android.arch.core.executor.testing.CountingTaskExecutorRule
import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.v7.widget.RecyclerView
import com.memrise.marvel.data.api.FakeMarvelApi
import com.memrise.marvel.repository.DataFactory
import com.memrise.marvel.ui.activity.ComicActivity
import com.memrise.marvel.util.Navigator
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

/**
 * Simple sanity test to ensure data is displayed
 */
@RunWith(Parameterized::class)
class CharacterActivityTest(val characterId: Int) {
    companion object {
        @JvmStatic
        @Parameterized.Parameters(name = "{0}")
        fun params() = listOf(1000)
    }
    @Suppress("MemberVisibilityCanPrivate")
    @get:Rule
    var testRule = CountingTaskExecutorRule()
    private val postFactory = DataFactory()
    @Before
    fun init() {
        val fakeApi = FakeMarvelApi()
        fakeApi.addComic(postFactory.getComics())
        fakeApi.addComic(postFactory.getComics())
        fakeApi.addComic(postFactory.getComics())
        val app = InstrumentationRegistry.getTargetContext().applicationContext as Application
    }

    @Test
    @Throws(InterruptedException::class, TimeoutException::class)
    fun showSomeResults() {
        val intent = Intent(InstrumentationRegistry.getTargetContext(), ComicActivity::class.java)
        intent.putExtra(Navigator.EXTRA_CHARACTER_ID, characterId)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val activity = InstrumentationRegistry.getInstrumentation().startActivitySync(intent)
        testRule.drainTasks(10, TimeUnit.SECONDS)
        val recyclerView = activity.findViewById<RecyclerView>(R.id.comic_recycler_view)
        MatcherAssert.assertThat(recyclerView.adapter, CoreMatchers.notNullValue())
        MatcherAssert.assertThat(recyclerView.adapter?.itemCount,
                CoreMatchers.`is`(3))
    }
}
