package com.memrise.marvel.data;

public enum Status {
    RUNNING,
    SUCCESS,
    FAILED
}
