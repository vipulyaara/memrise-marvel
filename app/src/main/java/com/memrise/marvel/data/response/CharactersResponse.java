package com.memrise.marvel.data.response;

import com.memrise.marvel.data.CharacterData;

public class CharactersResponse {

  public CharactersResponse(CharacterData data) {
    this.data = data;
  }

  private CharacterData data;

  public CharacterData getCharacterData() {
    return data;
  }
}
