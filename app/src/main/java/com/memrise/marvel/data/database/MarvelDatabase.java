package com.memrise.marvel.data.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.memrise.marvel.data.model.Character;
import com.memrise.marvel.data.model.Comic;

@Database(entities = {Character.class, Comic.class},
        exportSchema = false, version = 1)
public abstract class MarvelDatabase
        extends RoomDatabase {

    abstract public CharacterDao characterDao();

    abstract public ComicDao comicDao();
}
