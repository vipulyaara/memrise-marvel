package com.memrise.marvel.data.model;

import com.google.gson.annotations.SerializedName;

public class Prices {

  @SerializedName("type")
  private String type;
  @SerializedName("price")
  private double price;

  public Prices(String type, double price) {
    this.type = type;
    this.price = price;
  }

  public String getType() {
    return type;
  }

  public double getPrice() {
    return price;
  }
}
