package com.memrise.marvel.data.model

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.TypeConverters
import com.memrise.marvel.data.database.converters.ComicTypeConverters

@Entity(tableName = "comic", primaryKeys = arrayOf("id"),
        foreignKeys = [ForeignKey(entity = Character::class, parentColumns = arrayOf("id"), childColumns = arrayOf("characterId"))])
data class Comic(var id: Int?, val title: String, val description: String?, @field:TypeConverters
(ComicTypeConverters::class) val prices: List<Prices>?,
                 @field:Embedded val thumbnail: ComicThumbnail?, var characterId: Int) {
    var indexInResponse: Int = -1
}
