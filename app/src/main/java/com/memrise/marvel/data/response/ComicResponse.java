package com.memrise.marvel.data.response;

import com.memrise.marvel.data.ComicData;

public class ComicResponse {

  public ComicResponse(ComicData data) {
    this.data = data;
  }

  private ComicData data;

  public ComicData getComicData() {
    return data;
  }

}
