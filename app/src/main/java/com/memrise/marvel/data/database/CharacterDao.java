package com.memrise.marvel.data.database;

import android.arch.paging.DataSource;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.memrise.marvel.data.model.Character;

import java.util.List;

/**
 * Database access for [Character]s
 */
@Dao
public interface CharacterDao {
    @Query("SELECT * FROM Character ORDER BY name")
    DataSource.Factory<Integer, Character> getCharacters();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Character> results);

    @Query("SELECT MAX(indexInResponse) + 1 FROM character")
    int getNextIndexInCharacter();

    @Query("DELETE FROM character")
    int clearCharacters();

    @Query("DELETE FROM comic WHERE characterId = :characterId")
    int clearComicWithCharacter(int characterId);
}
