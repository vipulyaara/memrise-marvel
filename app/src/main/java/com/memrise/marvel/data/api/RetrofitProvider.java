package com.memrise.marvel.data.api;

import android.app.Application;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

import kotlin.jvm.internal.Intrinsics;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by VipulKumar on 17/08/18.
 * Utilities to setup and provide [Retrofit].
 */
public class RetrofitProvider {

    @NotNull
    public static Retrofit provideDefaultRetrofit(@NotNull Application context) {
        return (new Retrofit.Builder())
                .baseUrl("https://gateway.marvel.com:443/v1/public/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(provideOkHttpClient(context))
                .build();
    }

    private static OkHttpClient provideOkHttpClient(Application context) {
        int cacheSize = 20 * 1024 * 1024; // 20 MB
        Cache cache = new Cache(context.getCacheDir(), cacheSize);
        okhttp3.OkHttpClient.Builder builder =
                new okhttp3.OkHttpClient.Builder()
                        .readTimeout(60L, TimeUnit.SECONDS)
                        .connectTimeout(60L, TimeUnit.SECONDS);
        if (Environment.showNetworkLogs) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor((Interceptor) interceptor);
        }

        builder.addInterceptor(new GenericInterceptor());
        builder.cache(cache);
        OkHttpClient var10000 = builder.build();
        Intrinsics.checkExpressionValueIsNotNull(var10000, "builder.build()");
        return var10000;
    }
}
