package com.memrise.marvel.data;

import com.memrise.marvel.BuildConfig;

/**
 * Created by VipulKumar on 17/08/18.
 */
public interface NetworkConstants {

    String apiKey = BuildConfig.marvelApiKey;
    String privateKey = BuildConfig.marvelPrivateKey;
}
