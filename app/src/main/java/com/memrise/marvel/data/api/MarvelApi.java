package com.memrise.marvel.data.api;

import com.memrise.marvel.data.response.CharactersResponse;
import com.memrise.marvel.data.response.ComicResponse;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by VipulKumar on 17/08/18.
 * APIs interface for [Retrofit].
 */
public interface MarvelApi {
    @GET("characters")
    @NotNull
    Call<CharactersResponse> getCharacters(@Query("offset") int offset);

    @GET("characters/{characterId}/comics")
    @NotNull
    Call<ComicResponse> getComics(@Path("characterId") int characterId, @Query("offset") int offset);
}
