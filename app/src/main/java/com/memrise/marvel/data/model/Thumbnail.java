package com.memrise.marvel.data.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Thumbnail implements Parcelable {

  private String extension;
  private String path;

  public Thumbnail(String extension) {
    this.extension = extension;
  }

  public String getImageUrl() {
    String originalUrl =  path + "." + extension;
    return originalUrl.replace("http", "https");
  }

  public String getExtension() {
    return extension;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.extension);
    dest.writeString(this.path);
  }

  protected Thumbnail(Parcel in) {
    this.extension = in.readString();
    this.path = in.readString();
  }

  public static final Parcelable.Creator<Thumbnail> CREATOR = new Parcelable.Creator<Thumbnail>() {
    @Override public Thumbnail createFromParcel(Parcel source) {
      return new Thumbnail(source);
    }

    @Override public Thumbnail[] newArray(int size) {
      return new Thumbnail[size];
    }
  };
}
