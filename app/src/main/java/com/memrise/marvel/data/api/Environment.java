package com.memrise.marvel.data.api;

/**
 * Created by VipulKumar on 14/08/18.
 * Configuration class.
 */
public class Environment {
    public static boolean showNetworkLogs = true;
}
