package com.memrise.marvel.data.model;

import com.google.gson.annotations.SerializedName;

public class ComicThumbnail {

    @SerializedName("extension")
    private String extension;
    @SerializedName("path")
    private String path;

    public ComicThumbnail(String extension) {
        this.extension = extension;
    }

    public String getImageUrl() {
        String originalUrl = path + "." + extension;
        return originalUrl.replace("http", "https");
    }

    public String getExtension() {
        return extension;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
