package com.memrise.marvel.data.model;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;

@Entity(tableName = "character", primaryKeys = "id")
public class Character {

    private int id;
    private String name;
    private String description;
    @Embedded(prefix = "thumbnail_")
    private final Thumbnail thumbnail;

    private int indexInResponse = -1;

    public Character(int id, String name, String description, Thumbnail thumbnail) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.thumbnail = thumbnail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public int getIndexInResponse() {
        return indexInResponse;
    }

    public void setIndexInResponse(int indexInResponse) {
        this.indexInResponse = indexInResponse;
    }
}
