package com.memrise.marvel.data;

import android.support.annotation.Nullable;

import com.memrise.marvel.data.model.Comic;

import java.util.List;

public class ComicData {
    private List<Comic> results;
    private int total;
    private int count;

    public int getCount() {
        return count;
    }

    public int getTotal() {
        return total;
    }

    public List<Comic> getComics() {
        return results;
    }

    public ComicData(List<Comic> results) {
        this.results = results;
    }

    public ComicData(List<Comic> results, int totalCount,
                     @Nullable Integer next) {
        this.results = results;
        this.total = totalCount;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setResults(List<Comic> results) {
        this.results = results;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
