package com.memrise.marvel.data.api;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import javax.inject.Inject;
import javax.inject.Singleton;


// Not using this class now. As default fallback of this architecture is DB.
@Singleton
public class NetworkUtils {

    @Inject
    public NetworkUtils() {
    }

    private NetworkInfo networkInfo;

    private NetworkInfo retrieveNetworkInfo(Application application) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) application.getSystemService(
                        Context.CONNECTIVITY_SERVICE);

        if (connectivityManager != null) {
            return connectivityManager.getActiveNetworkInfo();
        }
        return null;
    }

    public boolean isConnected(Application application) {
        if (networkInfo == null) {
            networkInfo = retrieveNetworkInfo(application);
        }
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }
}
