package com.memrise.marvel.data;

import com.memrise.marvel.data.model.Character;

import java.util.List;

public class CharacterData {

    public CharacterData(List<Character> results) {
        this.results = results;
    }

    private List<Character> results;
    private int total;
    private int count;

    public List<Character> getCharacters() {
        return results;
    }

    public int getTotal() {
        return total;
    }

    public int getCount() {
        return count;
    }

    public void setResults(List<Character> results) {
        this.results = results;
    }
}

