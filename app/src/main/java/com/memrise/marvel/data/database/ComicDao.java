package com.memrise.marvel.data.database;

import android.arch.paging.DataSource;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.memrise.marvel.data.model.Comic;

import java.util.List;

/**
 * Database access for [Comic]s
 */
@Dao
public interface ComicDao {
    @Query("SELECT * FROM comic WHERE characterId = :characterId ORDER BY indexInResponse")
    DataSource.Factory<Integer, Comic> getComics(int characterId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Comic> comics);

    @Query("SELECT MAX(indexInResponse) + 1 FROM comic")
    int getNextIndexInCharacter();

    @Query("DELETE FROM comic WHERE characterId = :characterId")
    int clearComics(int characterId);

    @Query("DELETE FROM comic")
    int clearComics();
}
