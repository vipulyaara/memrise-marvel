package com.memrise.marvel.data.api;

import com.memrise.marvel.data.NetworkConstants;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by VipulKumar on 15/08/18.
 * Interceptor to pass auth parameters.
 */
public class GenericInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        String apiKey = NetworkConstants.apiKey;
        String privateKey = NetworkConstants.privateKey;
        String timeout = "1";
        String hash = md5(timeout + privateKey + apiKey);
        Request request = chain.request();
        HttpUrl url = request.url().newBuilder()
                .addQueryParameter("hash", hash)
                .addQueryParameter("apikey", apiKey)
                .addQueryParameter("ts", timeout)
                .build();
        request = request.newBuilder().url(url).method(request.method(), request.body()).build();
        return chain.proceed(request);
    }

    private static String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

}
