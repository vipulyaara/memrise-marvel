package com.memrise.marvel.data.database.converters;

import android.annotation.SuppressLint;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.util.StringUtil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.memrise.marvel.data.model.Prices;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * Room type converters for the module.
 */
public class ComicTypeConverters {
    private Gson gson = new Gson();

    @TypeConverter
    public List<Prices> PricesList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<Prices>>() {
        }.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public String PricesListToString(List<Prices> prices) {
        return gson.toJson(prices);
    }

    @SuppressLint("RestrictedApi")
    @TypeConverter
    public static List<Integer> stringToIntList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }
        return StringUtil.splitToIntList(data);
    }

    @SuppressLint("RestrictedApi")
    @TypeConverter
    public static String intListToString(List<Integer> ints) {
        return StringUtil.joinIntoString(ints);
    }
}
