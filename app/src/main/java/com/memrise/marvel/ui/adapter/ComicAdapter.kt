package com.memrise.marvel.ui.adapter

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.memrise.marvel.R
import com.memrise.marvel.data.NetworkState
import com.memrise.marvel.data.model.Comic
import com.squareup.picasso.Picasso

/**
 * A simple adapter implementation that shows Comics.
 */
class ComicAdapter : PagedListAdapter<Comic, RecyclerView.ViewHolder>(COMPARATOR) {
    private var networkState: NetworkState? = null

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (currentList?.size ?: 0 > 0) {
            when (getItemViewType(position)) {
                R.layout.item_comic -> {
                    val comic = getItem(position)
                    val comicHolder = holder as ComicAdapterHolder
                    comicHolder.title?.text = comic?.title
                    comicHolder.description?.text = comic?.description
                    Picasso.get().load(comic?.thumbnail?.imageUrl).into(comicHolder.image)
                    comicHolder.printPrice?.text = calculatePrices(comic)
                    comicHolder.digitalPurchasePrice?.text = calculatePrices(comic)
                }
            }
        }
    }

    private fun calculatePrices(comic: Comic?): String {
        return when {
            comic?.prices?.size == 1 -> java.lang.Double.toString(comic.prices[0].price)
            comic?.prices?.size == 2 -> java.lang.Double.toString(comic.prices[1].price)
            else -> "Price not available"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_comic -> ComicAdapterHolder.create(parent)
            R.layout.item_progress -> LoadingViewHolder.create(parent)
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        companion object {
            fun create(parent: ViewGroup): LoadingViewHolder {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_progress, parent, false)
                return LoadingViewHolder(view)
            }
        }
    }

    class ComicAdapterHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var title: TextView? = null
        internal var description: TextView? = null
        internal var image: ImageView? = null
        internal var printPrice: TextView? = null
        internal var digitalPurchasePrice: TextView? = null

        init {
            title = itemView.findViewById(R.id.comic_title)
            description = itemView.findViewById(R.id.comic_description)
            image = itemView.findViewById(R.id.comic_image)
            printPrice = itemView.findViewById(R.id.print_price)
            digitalPurchasePrice = itemView.findViewById(R.id.digital_purchase_price)
        }

        companion object {
            fun create(parent: ViewGroup): ComicAdapterHolder {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_comic, parent, false)
                return ComicAdapterHolder(view)
            }
        }
    }

    private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            R.layout.item_progress
        } else {
            R.layout.item_comic
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasExtraRow()) 1 else 0
    }

    fun setNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    companion object {
        val COMPARATOR = object : DiffUtil.ItemCallback<Comic>() {
            override fun areContentsTheSame(oldItem: Comic, newItem: Comic): Boolean =
                    oldItem == newItem

            override fun areItemsTheSame(oldItem: Comic, newItem: Comic): Boolean =
                    oldItem.id == newItem.id
        }
    }
}
