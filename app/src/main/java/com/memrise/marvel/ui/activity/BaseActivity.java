package com.memrise.marvel.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

public abstract class BaseActivity extends AppCompatActivity {
    public ViewGroup getViewGroup() {
        return (ViewGroup) findViewById(android.R.id.content);
    }
}
