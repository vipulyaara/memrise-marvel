package com.memrise.marvel.ui.activity;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.memrise.marvel.R;
import com.memrise.marvel.data.NetworkState;
import com.memrise.marvel.data.model.Character;
import com.memrise.marvel.ui.adapter.CharacterAdapter;
import com.memrise.marvel.util.AutoClearedValue;
import com.memrise.marvel.util.DeviceUtil;
import com.memrise.marvel.util.ImagePreviewerUtils;
import com.memrise.marvel.util.Navigator;
import com.memrise.marvel.util.transition.TransitionUtil;
import com.memrise.marvel.viewmodel.CharacterViewModel;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activity to fetch all the comics and show in a list.
 */
public class CharacterActivity extends BaseActivity {
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @BindView(R.id.snackbar_error_layout)
    ViewGroup snackbarErrorLayout;
    @BindView(R.id.tv_error_message_snackbar)
    TextView snackbarErrorMessage;
    @BindView(R.id.btn_retry_snackbar)
    TextView snackbarRetry;
    @BindView(R.id.error_layout)
    ViewGroup errorLayout;
    @BindView(R.id.tv_error_message)
    TextView errorMessage;
    @BindView(R.id.btn_retry)
    TextView retry;
    @BindView(R.id.character_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_collapsing)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.notification_layout)
    View notificationLayout;
    @BindView(R.id.fabBadge)
    FloatingActionButton fabBadge;

    private ViewGroup parent;

    // adapter is cleared automatically when activity is destroyed
    private AutoClearedValue<CharacterAdapter> characterAdapter;
    private CharacterViewModel characterViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);
        initViewModel();
        initView(getViewGroup());
    }

    private void initViewModel() {
        characterViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(CharacterViewModel.class);
        characterViewModel.characters.observe(this,
                characters -> characterAdapter.get().submitList(characters));

        characterViewModel.networkState.observe(this, this::onNetworkStateChanged);

        characterViewModel.fetchCharacters();
    }

    private void onNetworkStateChanged(NetworkState networkState) {
        characterAdapter.get().setNetworkState(networkState);
        switch (networkState.status) {
            case SUCCESS:
            case RUNNING:
                hideError();
                break;
            case FAILED:
                showError(networkState.msg);
                break;
        }
    }

    private void initView(ViewGroup parent) {
        ButterKnife.bind(this, parent);
        this.parent = parent;
        initToolbar();
        initCharacterAdapter();
        initSwipeToRefresh();

        retry.setOnClickListener(v -> characterViewModel.retry());
        snackbarRetry.setOnClickListener(v -> characterViewModel.retry());

        notificationLayout.setOnClickListener(v -> {
            Intent intent = new Intent(CharacterActivity.this,
                    NotificationActivity.class);
            if (!DeviceUtil.isPreLollipopDevice()) {
                ActivityOptions options = TransitionUtil.INSTANCE
                        .getFabExitTransitionBundle(CharacterActivity.this, intent, fabBadge);
                startActivity(intent, options.toBundle());
            } else {
                startActivity(intent);
            }
        });
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        collapsingToolbar.setTitle(getString(R.string.app_name));
        collapsingToolbar.setExpandedTitleTypeface(Typeface.DEFAULT_BOLD);
    }

    void initCharacterAdapter() {
        characterAdapter = new AutoClearedValue<>(this,
                new CharacterAdapter(new CharacterAdapter.ItemClickListener() {
                    @Override
                    public void onItemClicked(@NonNull Character character) {
                        Navigator.navigateToComicActivity(CharacterActivity.this,
                                character.getId(), character.getName());
                    }

                    @Override
                    public void onItemLongClicked(@NonNull Character character,
                                                  @NonNull View itemView) {
                        showQuickPreview(character, itemView);
                    }
                }));

        LinearLayoutManager layoutManager = new LinearLayoutManager(parent.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(characterAdapter.get());
    }

    private void initSwipeToRefresh() {
        characterViewModel.refreshState.observe(this, networkState ->
                swipeRefresh.setRefreshing(networkState == NetworkState.LOADING));
        swipeRefresh.setOnRefreshListener(() -> characterViewModel.refresh());
    }

    @SuppressLint("ClickableViewAccessibility")
    private void showQuickPreview(Character character, View itemView) {
        BitmapDrawable background = ImagePreviewerUtils.getBlurredScreenDrawable(this,
                parent.getRootView());

        @SuppressLint("InflateParams")
        View dialogView = LayoutInflater.from(this)
                .inflate(R.layout.view_zoomed_character, null);
        ImageView characterImage = dialogView.findViewById(R.id.character_image);
        TextView characterName = dialogView.findViewById(R.id.character_name);
        TextView characterDescription = dialogView.findViewById(R.id.character_description);

        Animation scaleAnimation = AnimationUtils.loadAnimation(this,
                R.anim.anim_scale_preview);
        scaleAnimation.setDuration(320);
        scaleAnimation.setInterpolator(new OvershootInterpolator());
        dialogView.startAnimation(scaleAnimation);

        Picasso.get().load(character.getThumbnail().getImageUrl()).into(characterImage);
        characterName.setText(character.getName());
        characterDescription.setText(character.getDescription());

        final Dialog dialog = new Dialog(this, R.style.TransparentDialog) {
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                if (getWindow() != null) {
                    getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.MATCH_PARENT);
                }
            }
        };
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(background);
        }
        dialog.setContentView(dialogView);
        dialog.show();

        itemView.setOnTouchListener((v, event) -> {
            if (dialog.isShowing()) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                int action = event.getActionMasked();
                if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    dialog.dismiss();
                    return true;
                }
            }
            return false;
        });
    }

    private void showError(String error) {
        if (characterAdapter.get().getItemCount() == 0) {
            errorLayout.setVisibility(View.VISIBLE);
            errorMessage.setText(error);
        } else {
            snackbarErrorLayout.setVisibility(View.VISIBLE);
            snackbarErrorMessage.setText(error);
        }
    }

    private void hideError() {
        errorLayout.setVisibility(View.GONE);
        snackbarErrorLayout.setVisibility(View.GONE);
    }
}
