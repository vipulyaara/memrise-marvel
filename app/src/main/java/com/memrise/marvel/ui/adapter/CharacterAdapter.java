package com.memrise.marvel.ui.adapter;

import android.arch.paging.PagedListAdapter;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.memrise.marvel.R;
import com.memrise.marvel.data.NetworkState;
import com.memrise.marvel.data.model.Character;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by VipulKumar on 17/08/18.
 * Adapter to hold characters.
 */
public class CharacterAdapter extends PagedListAdapter<Character, RecyclerView.ViewHolder> {
    private ItemClickListener itemClickListener;
    private NetworkState networkState;

    public CharacterAdapter(ItemClickListener itemClickListener) {
        super(COMPARATOR);
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case R.layout.item_character:
                return CharacterAdapterHolder.create(parent);
            case R.layout.item_progress:
                return LoadingViewHolder.create(parent);
            default:
                throw new IllegalArgumentException("unknown view type " + viewType);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getCurrentList() != null && getCurrentList().size() > 0) {
            if (getItemViewType(position) == R.layout.item_character) {
                Character character = getItem(position);
                CharacterAdapterHolder characterHolder = new
                        CharacterAdapterHolder(holder.itemView);
                if (character != null) {
                    characterHolder.name.setText(character.getName());
                    characterHolder.description.setText(character.getDescription());
                    Picasso.get().load(character.getThumbnail().getImageUrl())
                            .into(characterHolder.image);
                }
                holder.itemView.setOnClickListener(v -> itemClickListener.onItemClicked(character));
                holder.itemView.setOnLongClickListener(v -> {
                    itemClickListener.onItemLongClicked(character, holder.itemView);
                    return false;
                });
            }
        }
    }

    public static class CharacterAdapterHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.character_name)
        TextView name;
        @BindView(R.id.character_description)
        TextView description;
        @BindView(R.id.character_image)
        ImageView image;

        CharacterAdapterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public static CharacterAdapterHolder create(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_character, parent, false);
            return new CharacterAdapterHolder(view);
        }
    }

    public static class LoadingViewHolder extends RecyclerView.ViewHolder {
        LoadingViewHolder(View itemView) {
            super(itemView);
        }

        public static LoadingViewHolder create(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_progress, parent, false);
            return new LoadingViewHolder(view);
        }
    }

    private static DiffUtil.ItemCallback<Character> COMPARATOR = new DiffUtil
            .ItemCallback<Character>() {
        @Override
        public boolean areItemsTheSame(@NonNull Character oldItem, @NonNull Character newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Character oldItem, @NonNull Character newItem) {
            return oldItem.equals(newItem);
        }
    };


    private boolean hasExtraRow() {
        return networkState != null && networkState != NetworkState.LOADED;
    }

    @Override
    public int getItemViewType(int position) {
        if (hasExtraRow() && position == getItemCount() - 1) {
            return R.layout.item_progress;
        } else {
            return R.layout.item_character;
        }
    }

    /**
     * Add or remove progress bar.
     */
    public void setNetworkState(NetworkState newNetworkState) {
        NetworkState previousState = this.networkState;
        boolean hadExtraRow = hasExtraRow();
        this.networkState = newNetworkState;
        boolean hasExtraRow = hasExtraRow();
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount());
            } else {
                notifyItemInserted(super.getItemCount());
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(getItemCount() - 1);
        }
    }

    @Override
    public int getItemCount() {
        return super.getItemCount() + ((hasExtraRow()) ? 1 : 0);
    }

    public interface ItemClickListener {
        void onItemClicked(Character character);

        void onItemLongClicked(Character character, View itemView);
    }
}
