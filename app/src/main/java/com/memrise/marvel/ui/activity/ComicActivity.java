package com.memrise.marvel.ui.activity;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.memrise.marvel.R;
import com.memrise.marvel.data.NetworkState;
import com.memrise.marvel.ui.adapter.ComicAdapter;
import com.memrise.marvel.util.AutoClearedValue;
import com.memrise.marvel.util.Navigator;
import com.memrise.marvel.viewmodel.ComicViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ComicActivity extends BaseActivity {

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @BindView(R.id.snackbar_error_layout)
    ViewGroup snackbarErrorLayout;
    @BindView(R.id.tv_error_message_snackbar)
    TextView snackbarErrorMessage;
    @BindView(R.id.btn_retry_snackbar)
    TextView snackbarRetry;
    @BindView(R.id.error_layout)
    ViewGroup errorLayout;
    @BindView(R.id.tv_error_message)
    TextView errorMessage;
    @BindView(R.id.btn_retry)
    TextView retry;
    @BindView(R.id.comic_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_collapsing)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    private ViewGroup parent;

    private AutoClearedValue<ComicAdapter> comicAdapter;
    private ComicViewModel comicViewModel;

    private String characterName;
    private int characterId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comic);
        characterId = getIntent().getIntExtra(Navigator.EXTRA_CHARACTER_ID, 0);
        characterName = getIntent().getStringExtra(Navigator.EXTRA_CHARACTER_NAME);
        initViewModel();
        initView(getViewGroup());
    }

    @SuppressLint("VisibleForTests")
    private void initViewModel() {
        comicViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ComicViewModel.class);
        comicViewModel.comics.observe(this, comics -> comicAdapter.get().submitList(comics));

        comicViewModel.networkState.observe(this, this::onNetworkStateChanged);
    }

    private void onNetworkStateChanged(NetworkState networkState) {
        comicAdapter.get().setNetworkState(networkState);
        switch (networkState.status) {
            case SUCCESS:
            case RUNNING:
                hideError();
                break;
            case FAILED:
                showError(networkState.msg);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        comicViewModel.fetchComics(characterId);
    }

    private void initView(ViewGroup parent) {
        ButterKnife.bind(this, parent);
        this.parent = parent;
        initToolbar();
        initComicAdapter();
        initSwipeToRefresh();

        retry.setOnClickListener(v -> comicViewModel.retry());
        snackbarRetry.setOnClickListener(v -> comicViewModel.retry());
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitle(characterName);
        collapsingToolbar.setTitle(characterName);
        collapsingToolbar.setExpandedTitleTypeface(Typeface.DEFAULT_BOLD);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(characterName);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    void initComicAdapter() {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(parent.getContext());
        recyclerView.setLayoutManager(layoutManager);
        comicAdapter = new AutoClearedValue<>(this, new ComicAdapter());
        recyclerView.setAdapter(comicAdapter.get());
    }

    private void initSwipeToRefresh() {
        comicViewModel.refreshState.observe(this, networkState ->
                swipeRefresh.setRefreshing(networkState == NetworkState.LOADING));
        swipeRefresh.setOnRefreshListener(() -> comicViewModel.refresh());
    }

    private void showError(String error) {
        if (comicAdapter.get().getItemCount() == 0) {
            errorLayout.setVisibility(View.VISIBLE);
            errorMessage.setText(error);
        } else {
            snackbarErrorLayout.setVisibility(View.VISIBLE);
            snackbarErrorMessage.setText(error);
        }
    }

    private void hideError() {
        errorLayout.setVisibility(View.INVISIBLE);
        snackbarErrorLayout.setVisibility(View.INVISIBLE);
    }
}
