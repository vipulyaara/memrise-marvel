package com.memrise.marvel.ui.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ViewGroup;

import com.memrise.marvel.R;
import com.memrise.marvel.util.transition.TransitionUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Authored by vipulkumar on 04/08/17.
 * Activity to host a full screen dialog. Just to showcase a shared element transition.
 */

public class NotificationActivity extends BaseActivity {
    @BindView(R.id.root_layout)
    ViewGroup rootLayout;
    private Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        unbinder = ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionUtil.INSTANCE.setupFabEnterTransition(this, rootLayout);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        unbinder = null;
    }
}
