package com.memrise.marvel.paging;

import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedList;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;

import com.memrise.marvel.data.NetworkState;
import com.memrise.marvel.util.AppExecutors;

import org.jetbrains.annotations.NotNull;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VipulKumar on 17/08/18.
 * A generic implementation of {@link android.arch.paging.PagedList.BoundaryCallback}.
 *
 * This class is responsible for providing callbacks from paging library.
 * e.g. It signals when a PagedList has reached the end of available data.
 *
 * The class {@link PagingRequestHelper} is not a part of the paging library yet but it is going
 * to be in future, according to the developers. So it's safe to use it.
 */
public abstract class GenericBoundaryCallback<ItemType, ResponseType> extends
        PagedList.BoundaryCallback<ItemType> {

    @NotNull
    private final PagingRequestHelper helper;
    @NotNull
    private final LiveData<NetworkState> networkState;
    private final Function1<ResponseType, Unit> handleResponse;
    private final AppExecutors appExecutors;

    protected GenericBoundaryCallback(@NotNull Function1<ResponseType, Unit> handleResponse,
                                      @NotNull AppExecutors appExecutors) {
        this.handleResponse = handleResponse;
        this.appExecutors = appExecutors;
        this.helper = new PagingRequestHelper(this.appExecutors.diskIO());
        this.networkState = PagingRequestHelperExt.createStatusLiveData(this.helper);
    }

    @NotNull
    public PagingRequestHelper getHelper() {
        return helper;
    }

    @NotNull
    public final LiveData<NetworkState> getNetworkState() {
        return this.networkState;
    }

    @MainThread
    public void onZeroItemsLoaded() {
        this.helper.runIfNotRunning(PagingRequestHelper.RequestType.INITIAL,
                callback -> appExecutors.networkIO().execute(() -> fetchItems(callback)));
    }

    public abstract void fetchItems(@NotNull PagingRequestHelper.Request.Callback callback);

    @MainThread
    public void onItemAtEndLoaded(@NonNull final ItemType itemAtEnd) {
        this.helper.runIfNotRunning(PagingRequestHelper.RequestType.AFTER,
                callback -> appExecutors.networkIO().execute(() ->
                        fetchItemsWithPaging(itemAtEnd, callback)));
    }

    public abstract void
    fetchItemsWithPaging(ItemType item, @NotNull PagingRequestHelper.Request.Callback callback);

    public void onItemAtFrontLoaded(@NonNull ItemType itemAtFront) {
    }

    private void insertItemsIntoDb(final Response<ResponseType> response,
                                   final PagingRequestHelper.Request.Callback it) {
        this.appExecutors.diskIO().execute(() -> {
            handleResponse.invoke(response.body());
            it.recordSuccess();
        });
    }

    @NotNull
    protected final Callback<ResponseType>
    createWebserviceCallback(@NotNull final PagingRequestHelper.Request.Callback it) {
        return new Callback<ResponseType>() {
            public void onFailure(@NotNull Call<ResponseType> call, @NotNull Throwable t) {
                it.recordFailure(t);
            }

            public void onResponse(@NotNull Call<ResponseType> call, @NotNull Response<ResponseType>
                    response) {
                insertItemsIntoDb(response, it);
            }
        };
    }
}
