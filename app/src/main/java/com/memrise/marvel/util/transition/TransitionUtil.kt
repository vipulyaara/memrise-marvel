package com.memrise.marvel.util.transition

import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewCompat
import android.view.View
import com.memrise.marvel.R
import com.memrise.marvel.util.DeviceUtil

/**
 * Authored by vipulkumar on 22/06/17.
 */

object TransitionUtil {

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
//For Fab
    fun setupFabEnterTransition(activity: Activity, container: View) {
        if (!DeviceUtil.isPreLollipopDevice()) {
            if (!FabTransform.setup(activity, container)) {
                MorphTransform.setup(activity, container,
                        ContextCompat.getColor(activity, R.color.background),
                        activity.resources.getDimensionPixelSize(R.dimen.generic_margin_large))
            }
        }
    }

    //For Fab
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    fun getFabExitTransitionBundle(activity: Activity, intent: Intent, view: View): ActivityOptions {
        FabTransform.addExtras(intent, ContextCompat.getColor(
                activity, R.color.colorAccent), R.drawable.ic_notifications_black_24dp)
        return ActivityOptions.makeSceneTransitionAnimation(
                activity, view, ViewCompat.getTransitionName(view))
    }
}
