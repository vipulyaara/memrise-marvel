package com.memrise.marvel.util;

import android.os.Build.VERSION;

public final class DeviceUtil {

    private DeviceUtil() {
    }

    public static boolean isPreLollipopDevice() {
        return VERSION.SDK_INT < 21;
    }

    public static boolean isPreMarshmallowDevice() {
        return VERSION.SDK_INT < 21;
    }
}
