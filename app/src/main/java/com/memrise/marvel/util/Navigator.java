package com.memrise.marvel.util;

import android.content.Context;
import android.content.Intent;

import com.memrise.marvel.ui.activity.ComicActivity;

/**
 * Created by VipulKumar on 17/08/18.
 */
public class Navigator {
    public static String EXTRA_CHARACTER_ID = "extra_character_id";
    public static String EXTRA_CHARACTER_NAME = "extra_character_name";

    public static void navigateToComicActivity(Context context, int characterId,
                                               String characterName) {
        Intent intent = new Intent(context, ComicActivity.class);
        intent.putExtra(EXTRA_CHARACTER_ID, characterId);
        intent.putExtra(EXTRA_CHARACTER_NAME, characterName);
        context.startActivity(intent);
    }
}
