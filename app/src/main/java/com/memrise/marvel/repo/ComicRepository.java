package com.memrise.marvel.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.paging.DataSource;
import android.arch.paging.LivePagedListBuilder;
import android.support.annotation.MainThread;

import com.memrise.marvel.data.NetworkState;
import com.memrise.marvel.data.api.MarvelApi;
import com.memrise.marvel.data.model.Comic;
import com.memrise.marvel.data.response.ComicResponse;
import com.memrise.marvel.data.database.MarvelDatabase;
import com.memrise.marvel.paging.GenericBoundaryCallback;
import com.memrise.marvel.paging.Listing;
import com.memrise.marvel.paging.ListingKt;
import com.memrise.marvel.paging.PagingRequestHelper;
import com.memrise.marvel.util.AppExecutors;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import kotlin.Unit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VipulKumar on 17/08/18.
 * Repository to fetch and refresh comics.
 */
@Singleton
public class ComicRepository {
    private Application application;
    private AppExecutors appExecutors;
    private MarvelApi marvelApi;
    private MarvelDatabase db;

    @Inject
    public ComicRepository(Application application, AppExecutors appExecutors,
                           MarvelApi marvelApi, MarvelDatabase db) {
        this.application = application;
        this.appExecutors = appExecutors;
        this.marvelApi = marvelApi;
        this.db = db;
    }

    /**
     * Returns a Listing of all the comics.
     */
    @MainThread
    public Listing<Comic> getComics(int characterId) {
        // create a boundary callback which will observe when the user reaches to the edges of
        // the list and update the database with extra data.

        GenericBoundaryCallback<Comic, ComicResponse> boundaryCallback =
                new GenericBoundaryCallback<Comic, ComicResponse>(comicResponse ->
                        insertResultIntoDb(comicResponse, characterId),
                        appExecutors) {
                    @Override
                    public void fetchItems(@NotNull PagingRequestHelper.Request.Callback callback) {
                        marvelApi.getComics(characterId, 0)
                                .enqueue(createWebserviceCallback(callback));
                    }

                    @Override
                    public void fetchItemsWithPaging(Comic item, @NotNull PagingRequestHelper
                            .Request.Callback callback) {
                        marvelApi.getComics(characterId, item.getIndexInResponse())
                                .enqueue(createWebserviceCallback(callback));
                    }
                };

        // create a data source factory from Room
        DataSource.Factory<Integer, Comic> dataSourceFactory = db.comicDao().getComics(characterId);
        int comicPageSize = 10;
        LivePagedListBuilder<Integer, Comic> builder =
                new LivePagedListBuilder<>(dataSourceFactory, comicPageSize)
                        .setBoundaryCallback(boundaryCallback);

        // we are using a mutable live data to trigger refresh requests which eventually calls
        // refresh method and gets a new live data. Each refresh request by the user becomes a newly
        // dispatched data in refreshTrigger
        MutableLiveData<Void> refreshTrigger = new MutableLiveData<>();
        LiveData<NetworkState> refreshState = Transformations.switchMap(refreshTrigger,
                it -> refresh(characterId));

        return new Listing<>(builder.build(), boundaryCallback.getNetworkState(),
                refreshState,
                () -> {
                    refreshTrigger.setValue(null);
                    return null;
                },
                () -> {
                    boundaryCallback.getHelper().retryAllFailed();
                    return null;
                });
    }

    /**
     * When refresh is called, we simply run a fresh network request and when it arrives, clear
     * the database table and insert all new items in a transaction.
     *
     * Since the PagedList already uses a database bound data source, it will automatically be
     * updated after the database transaction is finished.
     */
    @MainThread
    private LiveData<NetworkState> refresh(int characterId) {
        MutableLiveData<NetworkState> networkState = new MutableLiveData<>();
        networkState.setValue(NetworkState.LOADING);
        marvelApi.getComics(characterId, 0).enqueue(
                new Callback<ComicResponse>() {
                    @Override
                    public void onResponse(Call<ComicResponse> call,
                                           Response<ComicResponse> response) {
                        appExecutors.diskIO().execute(() -> {
                            db.runInTransaction(() -> {
                                db.comicDao().clearComics(characterId);
                                insertResultIntoDb(response.body(), characterId);
                            });
                            // since we are in bg thread now, post the result.
                            networkState.postValue(NetworkState.LOADED);
                        });
                    }

                    @Override
                    public void onFailure(Call<ComicResponse> call, Throwable t) {
                        networkState.setValue(NetworkState.error(ListingKt.errorMessage
                                (application, t)));
                    }
                });
        return networkState;
    }

    /**
     * Inserts the response into the database while also assigning position indices to items.
     */
    private Unit insertResultIntoDb(ComicResponse body, int characterId) {
        if (body != null && body.getComicData() != null) {
            List<Comic> comics = body.getComicData().getComics();
            if (comics != null) {
                db.runInTransaction(() -> {
                    int start = db.comicDao().getNextIndexInCharacter();
                    for (Comic comic : comics) {
                        comic.setIndexInResponse(start + comics.indexOf(comic));
                        comic.setCharacterId(characterId);
                    }
                    db.comicDao().insert(comics);
                });
            }
        }
        return null;
    }
}
