package com.memrise.marvel.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.paging.DataSource;
import android.arch.paging.LivePagedListBuilder;
import android.support.annotation.MainThread;

import com.memrise.marvel.data.NetworkState;
import com.memrise.marvel.data.api.MarvelApi;
import com.memrise.marvel.data.model.Character;
import com.memrise.marvel.data.response.CharactersResponse;
import com.memrise.marvel.data.database.MarvelDatabase;
import com.memrise.marvel.paging.GenericBoundaryCallback;
import com.memrise.marvel.paging.Listing;
import com.memrise.marvel.paging.ListingKt;
import com.memrise.marvel.paging.PagingRequestHelper;
import com.memrise.marvel.util.AppExecutors;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import kotlin.Unit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VipulKumar on 17/08/18.
 */
@Singleton
public class CharacterRepository {
    private Application application;
    private AppExecutors appExecutors;
    private MarvelApi marvelApi;
    private MarvelDatabase db;

    @Inject
    CharacterRepository(Application application,
                        AppExecutors appExecutors, MarvelApi marvelApi, MarvelDatabase db) {
        this.application = application;
        this.appExecutors = appExecutors;
        this.marvelApi = marvelApi;
        this.db = db;
    }

    /**
     * Returns a Listing of all the characters.
     */
    @MainThread
    public Listing<Character> getCharacters() {
        // create a boundary callback which will observe when the user reaches to the edges of
        // the list and update the database with extra data.

        GenericBoundaryCallback<Character, CharactersResponse> boundaryCallback =
                new GenericBoundaryCallback<Character, CharactersResponse>(this::insertResultIntoDb,
                        appExecutors) {
                    @Override
                    public void fetchItems(@NotNull PagingRequestHelper.Request.Callback callback) {
                        marvelApi.getCharacters(0)
                                .enqueue(createWebserviceCallback(callback));
                    }

                    @Override
                    public void fetchItemsWithPaging(Character item, @NotNull PagingRequestHelper
                            .Request.Callback callback) {
                        marvelApi.getCharacters(item.getIndexInResponse())
                                .enqueue(createWebserviceCallback(callback));
                    }
                };

        // create a data source factory from Room
        DataSource.Factory<Integer, Character> dataSourceFactory = db.characterDao().getCharacters();
        int characterPageSize = 10;
        LivePagedListBuilder<Integer, Character> builder =
                new LivePagedListBuilder<>(dataSourceFactory, characterPageSize)
                        .setBoundaryCallback(boundaryCallback);

        // we are using a mutable live data to trigger refresh requests which eventually calls
        // refresh method and gets a new live data. Each refresh request by the user becomes a newly
        // dispatched data in refreshTrigger
        MutableLiveData<Void> refreshTrigger = new MutableLiveData<>();
        LiveData<NetworkState> refreshState = Transformations.switchMap(refreshTrigger,
                it -> refresh());

        return new Listing<>(builder.build(),
                boundaryCallback.getNetworkState(),
                refreshState,
                () -> {
                    refreshTrigger.setValue(null);
                    return null;
                },
                () -> {
                    boundaryCallback.getHelper().retryAllFailed();
                    return null;
                });
    }

    /**
     * When refresh is called, we simply run a fresh network request and when it arrives, clear
     * the database table and insert all new items in a transaction.
     *
     * Since the PagedList already uses a database bound data source, it will automatically be
     * updated after the database transaction is finished.
     */
    @MainThread
    private LiveData<NetworkState> refresh() {
        MutableLiveData<NetworkState> networkState = new MutableLiveData<NetworkState>();
        networkState.setValue(NetworkState.LOADING);
        marvelApi.getCharacters(0).enqueue(
                new Callback<CharactersResponse>() {
                    @Override
                    public void onResponse(Call<CharactersResponse> call,
                                           Response<CharactersResponse> response) {
                        appExecutors.diskIO().execute(() -> {
                            db.runInTransaction(() -> {
                                db.comicDao().clearComics();
                                db.characterDao().clearCharacters();
                                insertResultIntoDb(response.body());
                            });
                            // since we are in bg thread now, post the result.
                            networkState.postValue(NetworkState.LOADED);
                        });
                    }

                    @Override
                    public void onFailure(Call<CharactersResponse> call, Throwable t) {
                        networkState.setValue(NetworkState.error(ListingKt.errorMessage
                                (application, t)));
                    }
                });
        return networkState;
    }

    /**
     * Inserts the response into the database while also assigning position indices to items.
     */
    private Unit insertResultIntoDb(CharactersResponse body) {
        if (body != null && body.getCharacterData() != null) {
            List<Character> characters = body.getCharacterData().getCharacters();
            if (characters != null) {
                db.runInTransaction(() -> {
                    int start = db.characterDao().getNextIndexInCharacter();
                    for (Character character : characters) {
                        character.setIndexInResponse(start + characters.indexOf(character));
                    }
                    db.characterDao().insert(characters);
                });
            }
        }
        return null;
    }
}
