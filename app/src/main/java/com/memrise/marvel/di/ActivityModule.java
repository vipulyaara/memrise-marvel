package com.memrise.marvel.di;

import com.memrise.marvel.ui.activity.CharacterActivity;
import com.memrise.marvel.ui.activity.ComicActivity;
import com.memrise.marvel.ui.activity.NotificationActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Dagger module to provide all the activities.
 */
@Module
public abstract class ActivityModule {
    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract CharacterActivity contributeCharacterActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract ComicActivity contributeComicActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract NotificationActivity contributeNotificationActivity();
}
