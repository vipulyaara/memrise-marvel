package com.memrise.marvel.di;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.memrise.marvel.MarvelApplication;

import dagger.android.AndroidInjection;

/**
 * Helper class to automatically inject Activities.
 */
public class AppInjector {

    public static void init(MarvelApplication application) {
        DaggerAppComponent.builder().application(application)
                .build().inject(application);
        application
                .registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
                    @Override
                    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                        AndroidInjection.inject(activity);
                    }

                    @Override
                    public void onActivityStarted(Activity activity) {

                    }

                    @Override
                    public void onActivityResumed(Activity activity) {

                    }

                    @Override
                    public void onActivityPaused(Activity activity) {

                    }

                    @Override
                    public void onActivityStopped(Activity activity) {

                    }

                    @Override
                    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

                    }

                    @Override
                    public void onActivityDestroyed(Activity activity) {

                    }
                });
    }
}
