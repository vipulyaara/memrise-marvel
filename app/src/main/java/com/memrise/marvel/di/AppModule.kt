package com.memrise.marvel.di

import android.app.Application
import android.arch.persistence.room.Room
import android.support.annotation.Nullable
import com.memrise.marvel.data.api.MarvelApi
import com.memrise.marvel.data.api.NetworkUtils
import com.memrise.marvel.data.api.RetrofitProvider
import com.memrise.marvel.data.database.CharacterDao
import com.memrise.marvel.data.database.ComicDao
import com.memrise.marvel.data.database.MarvelDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Vipul Kumar.
 * Module for Data providers - both Remote and Local.
 */
@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideRetrofitService(application: Application): MarvelApi {
        return RetrofitProvider
                .provideDefaultRetrofit(application)
                .create(MarvelApi::class.java)
    }

    @Provides
    @Nullable
    fun provideEagerNetworkUtils(networkUtils: NetworkUtils): Void? {
        // a simple workaround for eager singleton in Dagger
        return null
    }


    @Singleton
    @Provides
    internal fun provideDb(app: Application): MarvelDatabase {
        val builder = Room.databaseBuilder(app,
                MarvelDatabase::class.java, "production")
//        if (Debug.isDebuggerConnected()) {
//            builder.allowMainThreadQueries() // only allow if debugging
//        }
        return builder.build()
    }

    @Singleton
    @Provides
    internal fun provideContentDao(db: MarvelDatabase): CharacterDao {
        return db.characterDao()
    }

    @Singleton
    @Provides
    internal fun provideContentDetailDao(db: MarvelDatabase): ComicDao {
        return db.comicDao()
    }
}
