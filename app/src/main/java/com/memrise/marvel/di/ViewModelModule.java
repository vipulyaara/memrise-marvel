package com.memrise.marvel.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.memrise.marvel.viewmodel.ViewModelFactory;
import com.memrise.marvel.viewmodel.CharacterViewModel;
import com.memrise.marvel.viewmodel.ComicViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Module to provide [ViewModel]s.
 */
@Module
public abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(CharacterViewModel.class)
    abstract ViewModel bindCharacterViewModel(CharacterViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ComicViewModel.class)
    abstract ViewModel bindComicViewModel(ComicViewModel viewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
