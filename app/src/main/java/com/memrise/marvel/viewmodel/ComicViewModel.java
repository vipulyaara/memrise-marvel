package com.memrise.marvel.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PagedList;

import com.memrise.marvel.data.NetworkState;
import com.memrise.marvel.data.model.Comic;
import com.memrise.marvel.paging.Listing;
import com.memrise.marvel.repo.ComicRepository;

import javax.inject.Inject;

import static android.arch.lifecycle.Transformations.map;
import static android.arch.lifecycle.Transformations.switchMap;

/**
 * Created by VipulKumar on 17/08/18.
 */
public class ComicViewModel extends BaseViewModel {
    private ComicRepository comicRepository;

    @Inject
    ComicViewModel(ComicRepository comicRepository) {
        this.comicRepository = comicRepository;
    }

    private MutableLiveData<Integer> characterId = new MutableLiveData<>();
    private LiveData<Listing<Comic>> repoResult = map(characterId,
            new Function<Integer, Listing<Comic>>() {
                @Override
                public Listing<Comic> apply(Integer input) {
                    return comicRepository.getComics(input);
                }
            });

    public LiveData<PagedList<Comic>> comics = switchMap(repoResult, Listing::getPagedList);
    public LiveData<NetworkState> networkState = switchMap(repoResult, Listing::getNetworkState);
    public LiveData<NetworkState> refreshState = switchMap(repoResult,
            Listing::getRefreshState);

    public void refresh() {
        if (repoResult != null && repoResult.getValue() != null) {
            repoResult.getValue().getRefresh().invoke();
        }
    }

    public void fetchComics(Integer id) {
        characterId.setValue(id);
    }

    public void retry() {
        Listing<Comic> listing = repoResult.getValue();
        if (listing != null) {
            listing.getRetry().invoke();
        }
    }
}
