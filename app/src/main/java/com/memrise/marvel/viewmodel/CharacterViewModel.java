package com.memrise.marvel.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PagedList;

import com.memrise.marvel.data.NetworkState;
import com.memrise.marvel.data.model.Character;
import com.memrise.marvel.paging.Listing;
import com.memrise.marvel.repo.CharacterRepository;

import javax.inject.Inject;

import static android.arch.lifecycle.Transformations.map;
import static android.arch.lifecycle.Transformations.switchMap;

/**
 * Created by VipulKumar on 17/08/18.
 */
public class CharacterViewModel extends BaseViewModel {
    private CharacterRepository characterRepository;

    @Inject
    CharacterViewModel(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    private MutableLiveData<Boolean> characterFetchBoolean = new MutableLiveData<>();
    private LiveData<Listing<Character>> repoResult = map(characterFetchBoolean,
            new Function<Boolean, Listing<Character>>() {
                @Override
                public Listing<Character> apply(Boolean input) {
                    return characterRepository.getCharacters();
                }
            });

    public LiveData<PagedList<Character>> characters = switchMap(repoResult, Listing::getPagedList);
    public LiveData<NetworkState> networkState = switchMap(repoResult, Listing::getNetworkState);
    public LiveData<NetworkState> refreshState = switchMap(repoResult,
            Listing::getRefreshState);

    public void refresh() {
        if (repoResult != null && repoResult.getValue() != null) {
            repoResult.getValue().getRefresh().invoke();
        }
    }

    public void fetchCharacters() {
        characterFetchBoolean.setValue(true);
    }

    public void retry() {
        Listing<Character> listing = repoResult.getValue();
        if (listing != null) {
            listing.getRetry().invoke();
        }
    }

}
