package com.memrise.marvel;

import com.memrise.marvel.data.model.Character;
import com.memrise.marvel.data.model.Thumbnail;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CharacterTest {

    public static final int HAL_9000_ID = 9000;
    public static final String HAL_9000_NAME = "Hal 9000";
    public static final String HAL_9000_DESCRIPTION =
            "I'm afraid you don't have the correct clearance";
    public static final Thumbnail HAL_9000_THUMBNAIL = null;

    public static Character hal9000 =
            new Character(HAL_9000_ID, HAL_9000_NAME, HAL_9000_DESCRIPTION, HAL_9000_THUMBNAIL);

    public static final int characterId = 999;
    public static final String characterName = "J.A.R.V.I.S";
    public static final String characterDescription = "You don't remember. Why I am not surprised?";
    public static final Thumbnail thumbnail = null;

    public static Character jarvis =
            new Character(characterId, characterName, characterDescription, thumbnail);

    public List<Character> getCharacters() {
        List<Character> characters = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Character character = jarvis;
            character.setId(i);
            characters.add(character);
        }
        return characters;
    }

    @Test
    public void testCharacterId() {
        Assert.assertNotNull(jarvis);
        Assert.assertNotEquals(jarvis.getId(), hal9000.getId());
        Assert.assertEquals(jarvis.getId(), 999);
    }

    @Test
    public void testCharacterName() {
        Assert.assertNotNull(jarvis);
        Assert.assertNotNull(jarvis.getName());
        Assert.assertNotEquals(jarvis.getName(), hal9000.getName());
        Assert.assertEquals(jarvis.getName(), "J.A.R.V.I.S");
    }

    @Test
    public void testCharacterDescription() {
        Assert.assertNotNull(jarvis);
        Assert.assertNotNull(jarvis.getDescription());
        Assert.assertNotEquals(jarvis.getDescription(), hal9000.getDescription());
        Assert.assertEquals(jarvis.getDescription(), "You don't remember. Why I am not surprised?");
    }

    @Test
    public void testCharacterThumbnail() {
        Assert.assertNotNull(jarvis);
        Assert.assertNull(jarvis.getThumbnail());
    }
}
