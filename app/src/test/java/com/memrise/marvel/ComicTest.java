package com.memrise.marvel;

import com.memrise.marvel.data.model.Comic;
import com.memrise.marvel.data.model.ComicThumbnail;
import com.memrise.marvel.data.model.Prices;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ComicTest {

  private static final int SPIDER_MAN_ID = 1011334;
  private static final Integer SPIDER_MAN_COMIC_ID = 22506;
  private static final String SPIDER_MAN_COMIC_TITLE =
      "Amazing Spider-Man (1999) #669 (Architect Variant)";
  private static final String SPIDER_MAN_COMIC_DESCRIPTION = "SPIDER-ISLAND PART THREE "
      + "How can the heroes of NYC hope to contain millions of spider-powered New Yorkers? "
      + "Also, a dangerous new role for Venom. And... is that a Six-armed Shocker?! "
      + "All this plus: one of Spider-Island's biggest mysteries revealed!";
  private static final List<Prices> SPIDER_MAN_COMIC_PRICE = new ArrayList<>();
  private static final ComicThumbnail thumbnail = null;
  private Prices spiderManPrice1 = new Prices("printPrice", 2.99);
  private Prices spiderManPrice2 = new Prices("digitalPurchasePrice", 1.99);
  private static int charcaterId = 1000;

  private Comic spider_man =
      new Comic(SPIDER_MAN_COMIC_ID, SPIDER_MAN_COMIC_TITLE, SPIDER_MAN_COMIC_DESCRIPTION,
          SPIDER_MAN_COMIC_PRICE, thumbnail, charcaterId);

  private static final int ALEX_POWER_ID = 1010836;
  private static final Integer ALEX_POWER_COMIC_ID = 65092;
  private static final String ALEX_POWER_COMIC_TITLE =
      "POWER PACK 63 CHRISTOPHER TRADING CARD VARIANT (2017) #63";
  private static final String ALEX_POWER_COMIC_DESCRIPTION = "WHERE IS POWER PACK? In a twist of "
      + "fate, four children gained incredible powers. And in a universe full of war-hungry aliens "
      + "and terrorizing gangsters, they would need them. Thus Power Pack was born! But when an "
      + "enemy from the past rears its head again, the youngest Power finds herself in a "
      + "body-snatching nightmare! Big brother Alex better come around — or Katie is toast! "
      + "A never-before-told adventure comes to light as Katie Power revisits family history!";
  private static final List<Prices> ALEX_POWER_COMIC_PRICE = new ArrayList<>();

  private Prices alexPowerPrice = new Prices("printPrice", 0.99);

  public static Comic alex_power =
      new Comic(ALEX_POWER_COMIC_ID, ALEX_POWER_COMIC_TITLE, ALEX_POWER_COMIC_DESCRIPTION,
          ALEX_POWER_COMIC_PRICE, thumbnail, charcaterId);


  private void createSpiderManPrices() {
    SPIDER_MAN_COMIC_PRICE.add(spiderManPrice1);
    SPIDER_MAN_COMIC_PRICE.add(spiderManPrice2);
  }

  private void createAlexPowerComicPrices() {
    ALEX_POWER_COMIC_PRICE.add(alexPowerPrice);
  }

  @Test public void testComic() {
    Assert.assertNotNull(spider_man);
  }

  @Test public void testCharacterId() {
    Assert.assertNotNull(SPIDER_MAN_ID);
    Assert.assertNotEquals(SPIDER_MAN_ID, ALEX_POWER_ID);
  }

  @Test public void testComicId() {
    Assert.assertNotNull(spider_man.getId());
    Assert.assertNotEquals(spider_man.getId(), alex_power.getId());
  }

  @Test public void testComicTitle() {
    Assert.assertNotNull(spider_man.getTitle());
    Assert.assertNotEquals(spider_man.getTitle(), alex_power.getTitle());
  }

  @Test public void testPrices() {
    createSpiderManPrices();
    createAlexPowerComicPrices();
    Assert.assertNotNull(spider_man.getPrices().get(0));
    Assert.assertNotNull(spider_man.getPrices().get(1));
    Assert.assertNotEquals(spider_man.getPrices().get(0), alex_power.getPrices().get(0));
    Assert.assertNotEquals(spider_man.getPrices().get(1), alex_power.getPrices().get(0));
  }
}
