package com.memrise.marvel.repository

import com.memrise.marvel.CharacterTest.*
import com.memrise.marvel.ComicTest.alex_power
import com.memrise.marvel.data.model.Character
import com.memrise.marvel.data.model.Comic
import java.util.*

class DataFactory {
    fun getCharacters(): List<Character> {
        val jarvis = Character(characterId, characterName, characterDescription, thumbnail)
        val characters = ArrayList<Character>()
        for (i in 0..9) {
            val character = jarvis
            character.id = i
            characters.add(character)
        }
        return characters
    }


    fun getComics(): Comic {
        return alex_power
    }

    fun getComicList(): MutableList<Comic> {
        val comics = ArrayList<Comic>()
        for (i in 0..9) {
            val comic = alex_power
            comic.id = i
            comics.add(comic)
        }
        return comics
    }
}
