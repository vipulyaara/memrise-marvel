package com.memrise.marvel.repository

import android.app.Application
import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import android.arch.paging.DataSource
import android.arch.paging.PagedList
import com.memrise.marvel.data.NetworkState
import com.memrise.marvel.data.api.FakeMarvelApi
import com.memrise.marvel.data.database.ComicDao
import com.memrise.marvel.data.database.MarvelDatabase
import com.memrise.marvel.data.model.Comic
import com.memrise.marvel.paging.Listing
import com.memrise.marvel.repo.ComicRepository
import com.memrise.marvel.util.CountingAppExecutors
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.mockito.Mockito
import org.mockito.Mockito.mock

@RunWith(Parameterized::class)
class ComicRepositoryTest(val characterId : Int) {
    companion object {
        @JvmStatic
        @Parameterized.Parameters(name = "{0}")
        fun params() = listOf(1000)
    }
    @Suppress("unused")
    @get:Rule // used to make all live data calls sync
    val instantExecutor = InstantTaskExecutorRule()
    private val fakeApi = FakeMarvelApi()
    private lateinit var db: MarvelDatabase
    private var countingAppExecutors: CountingAppExecutors? = CountingAppExecutors()
    private val postFactory = DataFactory()
    private lateinit var repository: ComicRepository
    private lateinit var application: Application

    @Before
    fun setup() {
        db = mock(MarvelDatabase::class.java)
        val comicDao = mock(ComicDao::class.java)
        val dataFactory = mock(DataSource.Factory::class.java)
        Mockito.`when`(comicDao.getComics(characterId)).thenReturn(dataFactory as DataSource.Factory<Int, Comic>?)
        repository = ComicRepository(application, countingAppExecutors?.appExecutors, fakeApi, db)
    }

    /**
     * asserts that empty list works fine
     */
    @Test
    fun emptyList() {
        val listing = repository.getComics(characterId)
        val pagedList = getPagedList(listing)
        assertThat(pagedList.size, `is`(0))
    }

    /**
     * asserts that a list w/ single item is loaded properly
     */
    @Test
    fun oneItem() {
        val post = postFactory.getComics()
        fakeApi.addComic(post)
        val listing = repository.getComics(characterId)
        assertThat(getPagedList(listing), `is`(listOf(post)))
    }

    /**
     * asserts loading a full list in multiple pages
     */
    @Test
    fun verifyCompleteList() {
        val posts = (0..10).map { postFactory.getComics() }
        posts.forEach(fakeApi::addComic)
        val listing = repository.getComics(characterId)
        // trigger loading of the whole list
        val pagedList = getPagedList(listing)
        pagedList.loadAround(posts.size - 1)
        assertThat(pagedList, `is`(posts))
    }

    /**
     * asserts the failure message when the initial load cannot complete
     */
    @Test
    fun failToLoadInitial() {
        fakeApi.failureMsg = "xxx"
        val listing = repository.getComics(characterId)
        // trigger load
        getPagedList(listing)
        assertThat(getNetworkState(listing), `is`(NetworkState.error("xxx")))
    }

    /**
     * asserts refresh loads the new data
     */
    @Test
    fun refresh() {
        val comics1 = (0..5).map { postFactory.getComics() }
        comics1.forEach(fakeApi::addComic)
        val listing = repository.getComics(characterId)
        val list = getPagedList(listing)
        list.loadAround(10)
        val comics2 = (0..10).map { postFactory.getComics() }
        fakeApi.clear()
        comics2.forEach(fakeApi::addComic)

        @Suppress("UNCHECKED_CAST")
        val refreshObserver = Mockito.mock(Observer::class.java) as Observer<NetworkState>
        listing.refreshState.observeForever(refreshObserver)
        listing.refresh()

        val list2 = getPagedList(listing)
        list2.loadAround(10)
        assertThat(list2, `is`(comics2))
        val inOrder = Mockito.inOrder(refreshObserver)
        inOrder.verify(refreshObserver).onChanged(NetworkState.LOADED) // initial state
        inOrder.verify(refreshObserver).onChanged(NetworkState.LOADING)
        inOrder.verify(refreshObserver).onChanged(NetworkState.LOADED)
    }

    /**
     * extract the latest paged list from the listing
     */
    private fun getPagedList(listing: Listing<Comic>): PagedList<Comic> {
        val observer = LoggingObserver<PagedList<Comic>>()
        listing.pagedList.observeForever(observer)
        assertThat(observer.value, `is`(notNullValue()))
        return observer.value!!
    }

    /**
     * extract the latest network state from the listing
     */
    private fun getNetworkState(listing: Listing<Comic>) : NetworkState? {
        val networkObserver = LoggingObserver<NetworkState>()
        listing.networkState.observeForever(networkObserver)
        return networkObserver.value
    }

    /**
     * simple observer that logs the latest value it receives
     */
    private class LoggingObserver<T> : Observer<T> {
        var value : T? = null
        override fun onChanged(t: T?) {
            this.value = t
        }
    }
}
