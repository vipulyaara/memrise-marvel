package com.memrise.marvel.data.api

import com.memrise.marvel.CharacterTest
import com.memrise.marvel.data.CharacterData
import com.memrise.marvel.data.ComicData
import com.memrise.marvel.data.model.Comic
import com.memrise.marvel.data.response.CharactersResponse
import com.memrise.marvel.data.response.ComicResponse
import com.memrise.marvel.repository.DataFactory
import retrofit2.Call
import retrofit2.mock.Calls
import java.io.IOException

/**
 * implements the MarvelApi with controllable requests
 */
class FakeMarvelApi : MarvelApi {
    var failureMsg: String? = null
    private val factory = DataFactory()

    override fun getCharacters(offset: Int): Call<CharactersResponse> {
        failureMsg?.let {
            return Calls.failure(IOException(it))
        }
        val list = CharacterTest().characters
        val response = CharactersResponse(CharacterData(list))
        return Calls.response(response)
    }

    override fun getComics(characterId: Int, offset: Int): Call<ComicResponse> {
        failureMsg?.let {
            return Calls.failure(IOException(it))
        }
        val list = factory.getComicList()
        val response = ComicResponse(ComicData(list))
        return Calls.response(response)
    }

    private val model = mutableMapOf<Int, Comics>()

    fun addComic(comic: Comic) {
        val comics = factory.getComicList()
        comics.add(comic)
    }

    private class Comics(val items: MutableList<Comic> = arrayListOf()) {
        fun findPosts(): List<Comic> {
            return items
        }
    }

    fun clear() {
        model.clear()
    }
}
